-- On crée une table Post
CREATE TABLE Post( 
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, 
    title VARCHAR(64), 
    postDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
    content VARCHAR(128),
    author VARCHAR(68) 
);
INSERT INTO Post (title, content, author) VALUES ('pas content','bonjour à tous je test des trucs', 'flo');
INSERT INTO Post (title, content, author) VALUES ('coucou','bonjour je suis fabien et je suis heureux', 'fabien');
INSERT INTO Post (title, content, author) VALUES ('Paula', 'dernier test pour gill post', 'gill');

SELECT * FROM Post;