<?php

namespace App\Controller;

use App\Repository\PostRepository;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{

    
   /**
    * @Route("/del/{id}", name="deletePost")
    */
   public function deletePost(int $id)
   {
      $repo = new PostRepository();
      $repo->delete($id);
      return $this->redirectToRoute('blog_post');
   }


    /**
     * 
     * @Route("/blog-form", name="blog_post")
     */
    public function index(PostRepository $repo) { //On injecte notre DAO
        //On l'utilise pour faire un findAll pour aller chercher toutes
        //les personnes présentent en bdd
        $newPost = $repo->findAll();
        //On donne les personnes au template twig
        return $this->render('blog-form.html.twig', [
            'post' => $newPost
        ]);
    }

    /**
     * @Route("/post/{id}", name="id")
     */
    public function onePost(int $id, PostRepository $repo){

        $post = $repo->findById($id);

        return $this->render('post-id.html.twig', [
            'post' => $post
        ]);
    }

    /**
     * @Route("/blog-test", name="blog_test")
     */
    public function addPost(Request $request)
    {
        
            $post = null;
            $title = $request->get("title");
            $author = $request->get("author");
            $image = $request->get("image");  
            $content = $request->get("content");
            
            if ($title && $author && $content) {
         
                $post = new Post($title, $author, $image, $content);           
                $postRepository = new PostRepository();
                $postRepository->add($post);
            }
    
            return $this->render('blog.html.twig', [
                'post' => $post
            ]);
        
    }
}
