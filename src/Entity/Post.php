<?php

namespace App\Entity;


class Post
{
    private $id;
    private $title;
    private $author;
    private $content;
    private $picture;
    private $postDate;
  

    public function __construct(string $title = "",  string $author = "", string $content = "", string $picture = "", int $id = null, string $postDate=" ")
    {
        
        $this->id = $id;
        $this->title = $title;     
        $this->author = $author;
        $this->content = $content;
        $this->picture = $picture;
        $this->postDate = $postDate;
       
        

    }


    public function getTitle(): string
    {
        return $this->title;
    }
    public function setTitle(string $title)
    {
        $this->title = $title;
    }
    public function getAuthor(): string
    {
        return $this->author;
    }
    public function setAuthor(string $author)
    {
        $this->author = $author;
    }
    public function getContent(): string
    {
        return $this->content;
    }
    public function setContent(string $content)
    {
        $this->content = $content;
    }
    public function getPicture(): string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): void
    {
        $this->picture = $picture;
    }

    public function getPostDate(): string
    {
        return $this->postDate;
    }

    public function setPostDate(string $postDate): void 
    {
        $this->postDate= $postDate;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

   
}
