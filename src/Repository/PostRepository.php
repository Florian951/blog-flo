<?php

namespace App\Repository;

use App\Entity\Post;
/**
 * Un Repository, ou DAO (terme plus "pro", Data Access Object) est une
 * class dont le but est de concentré les appels à la base de données,
 * ainsi, le reste de l'application dépendra des DAO et on aura pas 
 * des appels bdd disseminés partout dans l'appli (comme ça, si jamais 
 * la table change, on le sgbd ou autre, on aura juste le DAO à 
 * modifier et pas tous les endroits où on aurait fait ces appels)
 */
class PostRepository 
{
    private $pdo;

    public function __construct() {
        
        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'].';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD']
        );
    }

    public function add(Post $post): void {
        
        $query = $this->pdo->prepare('INSERT INTO Post (title,author,content,picture) VALUES (:title,:author,:content,:picture)');
       
        $query->bindValue(':title',$post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue(':author',$post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue(':content',$post->getContent(), \PDO::PARAM_STR);  
        $query->bindValue(':picture',$post->getPicture(), \PDO::PARAM_STR);  
        $query->execute();
        $post->setId(intval($this->pdo->lastInsertId()));

    }

    public function findById(int $id): ?Post {
        $query = $this->pdo->prepare('SELECT * FROM Post WHERE id=:idPlaceholder');

        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        $query->execute();
        $line = $query->fetch();
        if($line) {
            return $this->sqlToPost($line);
        }
        return null;
    }

    public function delete(int $id) :void{
        $query = $this->pdo->prepare('DELETE FROM Post WHERE id = :id');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->execute();
    }

   
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM Post');
        $query->execute();
        $results = $query->fetchAll();
        $list = [];

        foreach ($results as $line) {
            $post = $this->sqlToPost($line);
            $list[] = $post;
        }
        return $list;
    }
     /**
     * Methode dont le but est de transformer une ligne de résultat
     * PDO en instance de la classe Person
     * Cette méthode est juste là dans un but de refactorisation afin
     * d'éviter la répétition dans les différents find
     */

    private function sqlToPost(array $line): Post {
        return new Post( $line['title'], $line['author'], $line['picture'], $line['content'], $line['id'], $line['postDate']);
    }
}